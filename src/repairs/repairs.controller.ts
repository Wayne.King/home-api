import { RepairsService } from './repairs.service';
import {
    Controller,
    Get,
    Param,
    Post,
    Body,
    Delete,
    HttpCode
} from '@nestjs/common';
import { Repairs as RepairsModel } from '@prisma/client';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Repairs')
@Controller('repairs')
export class RepairsController {

    constructor (
        private readonly repairsService: RepairsService,
    ) {}

    @Get(':id')
    async getRepair(@Param('id') id: String): Promise<RepairsModel> {
        return this.repairsService.getRepair({id: Number(id)});
    }

    @Get('')
    async getRepairs(): Promise<RepairsModel[]> {
        return this.repairsService.getRepairs();
    }

    @Post('')
    async createRepair(
        @Body() repairData: {name: string, description?: string, price: string}
    ): Promise<RepairsModel> {
        const { name, description, price } = repairData;

        return this.repairsService.createRepair({
            name,
            description,
            price,
        });
    }

    @Delete(':id')
    @HttpCode(204)
    async deleteRepair(@Param('id') id: string): Promise<RepairsModel> {
        return this.repairsService.deleteRepair({ id: Number(id) });
    }
}
