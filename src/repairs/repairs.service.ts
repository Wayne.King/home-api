import { Injectable } from '@nestjs/common';
import { PrismaService } from 'prisma/prisma.service';
import { Repairs, Prisma } from '@prisma/client';

@Injectable()
export class RepairsService {
    constructor(private prisma: PrismaService) {}

    async getRepair(
        repairsWhereUniqueInput: Prisma.RepairsWhereUniqueInput
    ): Promise<Repairs | null> {
        return this.prisma.repairs.findUnique({
            where: repairsWhereUniqueInput,
        });
    }

    async getRepairs(): Promise<Repairs[] | null> {
        return this.prisma.repairs.findMany();
    }

    async createRepair(data: Prisma.RepairsCreateInput): Promise<Repairs | null> {
        return this.prisma.repairs.create({
            data,
        });
    }

    async deleteRepair(where: Prisma.RepairsWhereUniqueInput): Promise<Repairs | null> {
        return this.prisma.repairs.delete({
            where,
        });
    }
}
