import { Module } from '@nestjs/common';
import { PrismaService } from 'prisma/prisma.service';
import { RepairsController } from './repairs.controller';
import { RepairsService } from './repairs.service';

@Module({
  controllers: [RepairsController],
  providers: [RepairsService, PrismaService]
})
export class RepairsModule {}
