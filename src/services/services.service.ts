import { Injectable } from '@nestjs/common';
import { Prisma, Services } from '@prisma/client';
import { PrismaService } from 'prisma/prisma.service';

@Injectable()
export class ServicesService {
    constructor(private prisma: PrismaService) {}

    async getService(
        servicesWhereUniqueInput: Prisma.ServicesWhereUniqueInput
    ): Promise<Services | null> {
        return this.prisma.services.findUnique({
            where: servicesWhereUniqueInput,
        });
    }

    async getServices(): Promise<Services[] | null> {
        return this.prisma.services.findMany();
    }

    async createService(data: Prisma.ServicesCreateInput): Promise<Services | null> {
        return this.prisma.services.create({
            data,
        });
    }

    async deleteService(where: Prisma.ServicesWhereUniqueInput): Promise<Services | null> {
        return this.prisma.services.delete({
            where,
        });
    }
}
