import { ServicesService } from './services.service';
import { 
    Controller,
    Get,
    Param,
    Post,
    Body,
    Delete,
    HttpCode
} from '@nestjs/common';
import { Services as ServicesModel } from '@prisma/client';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Services')
@Controller('services')
export class ServicesController {
    constructor(
        private readonly servicesService: ServicesService,
    ) {}

    @Get(':id')
    async getService(@Param('id') id: String): Promise<ServicesModel> {
        return this.servicesService.getService({id: Number(id)});
    }

    @Get('')
    async getServices(): Promise<ServicesModel[]> {
        return this.servicesService.getServices();
    }

    @Post('')
    async createService(
        @Body() serviceData: {name: string, description?: string}
    ): Promise<ServicesModel> {
        const { name, description } = serviceData;

        return this.servicesService.createService({
            name,
            description,
        });
    }

    @Delete(':id')
    @HttpCode(204)
    async deleteService(@Param('id') id: string): Promise<ServicesModel> {
        return this.servicesService.deleteService({ id: Number(id) });
    }
}
