import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RepairsModule } from './repairs/repairs.module';
import { ServicesModule } from './services/services.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { RepairsController } from './repairs/repairs.controller';
import { ServicesController } from './services/services.controller';
import { RepairsService } from './repairs/repairs.service';
import { ServicesService } from './services/services.service';
import { PrismaService } from 'prisma/prisma.service';

@Module({
  imports: [RepairsModule, ServicesModule, ServeStaticModule.forRoot({
    rootPath: join(__dirname, '..', 'src/public/'),
  })],
  controllers: [AppController, RepairsController, ServicesController],
  providers: [AppService, RepairsService, ServicesService, PrismaService],
})
export class AppModule {}
